<?php

  interface iProduct {
    public function getTitle();
    public function setTitle($title);
    public function getDiscount();
    public function setDiscount($discount);
    public function getPrice();
    public function setPrice($price);
    public function getDelivery();
    public function setDelivery($delivery);
    public function showInfo();
  }

  interface iComputer extends iProduct {

  }

  interface iPotato extends iProduct {
    public function setWeight($weight);
    public function getWeight();
  }

  interface iShirt extends iProduct {
    public function getSize();
    public function setSize($size);
  }


  abstract class Product implements iProduct {
    private $title;
    private $price;
    private $discount;
    private $delivery = 250;
    
    public function getTitle() {
      return $this->title;
    }

    public function setTitle($title) {
      $this->title = $title;
      return $this;
    }

    public function getDiscount() {
      return $this->discount;
    }

    public function setDiscount($discount) {
      $this->discount = $discount;
      if ($this->discount > 0) {
        $this->setDelivery(300);
      }
      return $this;
    }

    public function getPrice() {
      return $this->price - ($this->price * $this->getDiscount() / 100);
    }

    public function setPrice($price) {
      $this->price = $price;
      return $this;
    }

    public function getDelivery() {
      return $this->delivery;
    }

    public function setDelivery($delivery) {
      $this->delivery = $delivery;
      return $this;
    }

    public function showInfo() {
      echo 'Наименование: '. $this->getTitle() . '<br>';
      $discount = $this->getDiscount() ? ' С учетом скидки ' . $this->getDiscount() . '%' : '';
      echo 'Окончательная цена: '. $this->getPrice() . $discount . '<br>';
      echo 'Стоимость доставки: '. $this->getDelivery() . '<br>';
      echo '<br>';
    }
  }

  final class Computer extends Product implements iComputer {
    function __construct() {
      $this->setDiscount(10);
    }
  }

  final class Potato extends Product implements iPotato {
    private $weight;

    public function setWeight($weight) {
      $this->weight = $weight;
      if ($this->weight > 10) {
        $this->setDiscount(10);
      }
      return $this;
    }

    public function getWeight() {
      return $this->weight;
    }
  }


  final class Shirt extends Product implements iShirt {
    private $size;

    public function getSize() {
      return $this->size;
    }

    public function setSize($size) {
      $this->size = $size;
      return $this;
    }
  }
  
  (new Computer())
    ->setTitle('Компьютер')
    ->setPrice(200)
    ->showInfo();

  (new Potato())
    ->setTitle('Картошка')
    ->setPrice(100)
    ->setWeight(11)
    ->showInfo();

  (new Shirt())
    ->setTitle('Рубашка')
    ->setPrice(20)
    ->setSize(42)
    ->showInfo();